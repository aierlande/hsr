﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HSR.Startup))]
namespace HSR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
